import { createRoutine } from 'redux-saga-routines';

export const fetchMainInfo = createRoutine('MAIN_INFO');
