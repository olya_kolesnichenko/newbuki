import callWebApi from '../helpers/webApiHelper';

export const getMainInfo = async () => {
  const response = await callWebApi({
    endpoint: '/main',
    type: 'GET'
  });
  return response.json();
};
