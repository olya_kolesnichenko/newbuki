import { all } from 'redux-saga/effects';

import mainSagas from '../scenes/MainPage/sagas';

export default function* rootSaga() {
  yield all([
    mainSagas()
  ]);
}
