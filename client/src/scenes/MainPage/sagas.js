import { takeEvery, put, call, all } from 'redux-saga/effects';
import * as mainService from '../../services/mainService';
import {
  fetchMainInfo
} from '../../routines/routines';

function* mainInfoRequest() {
  try {
    yield put(fetchMainInfo.request());
    const response = yield call(mainService.getMainInfo);
    yield put(fetchMainInfo.success(response));
  } catch (error) {
    yield put(fetchMainInfo.failure(error.message));
  } finally {
    yield put(fetchMainInfo.fulfill());
  }
}

function* watchMainInfoRequest() {
  yield takeEvery(fetchMainInfo.TRIGGER, mainInfoRequest);
}


export default function* profileSagas() {
  yield all([
    watchMainInfoRequest(),
  ]);
}
