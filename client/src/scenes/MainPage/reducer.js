import {fetchMainInfo} from '../../routines/routines';

const initialState = {
  data: '',
  loading: true,
  error: null
};

export default (state = initialState, action) => {
  switch (action.type) {
  case fetchMainInfo.TRIGGER:
    return {
      ...state,
      loading: true,
      error: null,
    };
  case fetchMainInfo.SUCCESS:
    return {
      ...state,
      data: action.payload.data
    };
  case fetchMainInfo.FAILURE: {
    return {
      ...state,
      error: action.payload
    };
  }
  case fetchMainInfo.FULFILL:
    return {
      ...state,
      loading: false
    };

  default:
    return state;
  }
};
