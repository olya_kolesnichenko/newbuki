import React, {Component} from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { fetchMainInfo } from '../../routines/routines';
import { Grid } from 'semantic-ui-react';
import Spinner from '../../components/Spinner';
import styles from './styles.module.scss';

class MainPage extends Component {

  componentDidMount() {
    this.props.fetchMainInfo();
  }

  render() {
    const { loading, text } = this.props;

    return loading ? (
      <Spinner />
    ) : (
      <div>
        <section className={styles.main}>
          <Grid centered container columns={1}>
            <Grid.Column computer={13} mobile={16}>
              <h1 className={styles.headerCentered}>BUKI</h1>
              <p className={styles.mainText}>
                {text}
              </p>
            </Grid.Column>
          </Grid>
        </section>

        <footer className={styles.footer}>
          <Grid className={styles.footerLine} container>
            <Grid.Row computer={13} mobile={16}>
              <p>Copyright © 2019</p>
            </Grid.Row>
          </Grid>
        </footer>
      </div>);
  }
}


MainPage.propTypes = {
  loading: PropTypes.bool.isRequired,
  text: PropTypes.string.isRequired,
  fetchMainInfo: PropTypes.func.isRequired
};

const mapStateToProps = ({
                           main: {
                             loading,
                             data
                           }
                         }) => ({
  loading,
  text: data
});

const mapDispatchToProps = {
  fetchMainInfo
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(MainPage);
