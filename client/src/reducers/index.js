import { combineReducers } from 'redux';

import main from '../scenes/MainPage/reducer';

export default combineReducers({
  main
});
