var express = require('express');
var app = express();

app.get('/', function (req, res) {
  res.send('Hello World!');
});
app.get('/main', function (req, res) {
  res.status(200).json({data: 'Hello! In BUKI we trust'});
});
app.listen(3000, function () {
  console.log('Example app listening on port 3000!');
});

